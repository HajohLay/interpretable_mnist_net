import tensorflow as tf
import numpy as np
import cv2
import scipy as sp

from tensorflow.keras.applications.inception_v3 import preprocess_input

class_name_to_idx = {'nv':2, 'mel':1, 'bkl':0}
class_idx_to_name = {2:'nv', 1:'mel', 0:'bkl'}

def load_cropped_image(image_path):
    img = cv2.imread(image_path)
    #state crop and original dims
    crop_width = 256
    crop_height = 256
    width, height = img.shape[1], img.shape[0]
    
    #determine center of original image half crop width / height
    mid_x, mid_y = int(width/2), int(height/2)
    cw2, ch2 = int(crop_width/2), int(crop_height/2)
    
    crop_img = img[mid_y-ch2:mid_y+ch2, mid_x-cw2:mid_x+cw2]
    return cv2.cvtColor(crop_img, cv2.COLOR_BGR2RGB)

def load_CAM_image(image_path):
    img = cv2.imread(image_path)
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB) 

def preproc(img):
    return preprocess_input(img)

class MNIST_net():

    def __init__(self, model_path):
        sess = tf.Session()
        tf.keras.backend.set_session(sess)
        init_op = tf.global_variables_initializer()
        sess.run(init_op)
        self.sess = sess
        self.graph = sess.graph
        self.model = tf.keras.models.load_model(model_path)
    
    def print_ops(self):
        for op in self.graph.get_operations():
            print(op.name)
    
    def summary(self):
        self.model.summary()
    
    def get_activations(self, image_dir, target_name='mixed9/concat:0',):
        target =  self.graph.get_tensor_by_name(target_name)
        image_tensor = self.graph.get_tensor_by_name('input_1:0')
        img = load_cropped_image(image_dir)
        img = preproc(img)

        activations = self.sess.run([target], feed_dict={image_tensor:[img]})
        max_activation = np.max(activations)
        return activations[0][0], max_activation
    
    def get_activations_for_dict(self, image_dir, target_name='mixed9/concat:0',):
        activations, max_activation = self.get_activations(target_name, image_dir)
        activations = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-5:])] for score_vec in score_slice] for score_slice in activations]
        return activations, max_activation
    
    def get_scores(self, target_name, image_dir):
        '''
        target name something like 'mixed9/concat:0'

        returns a dictionary of scores and the max score
        '''
        target = self.graph.get_tensor_by_name(target_name)
        image_tensor = self.graph.get_tensor_by_name('input_1:0')
        out_tensor = self.graph.get_tensor_by_name('softmax/Softmax:0')
        img = load_cropped_image(image_dir)
        img = preproc(img)

        #compute grads
        class_grads = {}
        for i in [0,1,2]:
            one_hot = tf.sparse_to_dense(i, [3], 1.0)
            signal = tf.multiply(out_tensor, one_hot)
            loss = tf.reduce_mean(signal)
            grads = tf.gradients(loss, target)[0]
            class_grads[i] = grads
        
        #compute acts and score
        scores = {}
        max_score = -10e10
        for i in [0,1,2]:
            output, grads_val = self.sess.run([target, class_grads[i]], feed_dict={image_tensor:[img]})
            class_score = np.multiply(output, grads_val)[0]
            max_score = max(np.max(class_score),max_score)
            scores[i] = class_score
        
        return scores, max_score
    
    def get_scores_for_dict(self, image_dir, target_name):
        scores, max_score = self.get_scores(target_name, image_dir)
        dict_scores = {}
        for i in range(3):
            dict_scores[i] = [[[{"n": n.tolist(), "v": float(score_vec[n])} for n in np.flip(np.argsort(score_vec)[-5:])] for score_vec in score_slice] for score_slice in scores[i]]
        return dict_scores, max_score
    
    def get_prediction(self, image_dir):
        '''
        return the models prediction for an input image
        '''
        img = load_cropped_image(image_dir)
        img = preproc(img)
        return self.model.predict([[img]])[0]
