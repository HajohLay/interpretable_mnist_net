import numpy as np
import tensorflow as tf
import os
import math

from MNIST_net_util import load_cropped_image, MNIST_net, preproc


if __name__ == "__main__":
    model = MNIST_net("models/MNIST_net_graph_unfolded.h5")
    # test_image = load_cropped_image("dataset/test/bkl/ISIC_0024446.jpg")
    # preproccess_image = preproc(test_image)
    print(np.shape(model.get_activations("dataset/test/bkl/ISIC_0024446.jpg")[0]))
    print("here")